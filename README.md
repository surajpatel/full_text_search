# Full text Search
* A search that compares every word in a document, as opposed to searching an abstract or a set of keywords associated with the document. Word processors and text editors contain full-text search functions that let you find a word or phrase anywhere in the document.
* Full-text searching is the type performed by most Web search engines on the Web pages that have been retrieved and added to their vast reservoirs. All the words on the pages are searched and then indexed, and the user's search request is satisfied via the indexes.
