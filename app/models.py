from app import db
from app.search import add_to_index, remove_from_index, query_index

class Post(db.Model):
    __searchable__ = ['posts','username']
    
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True)
    posts = db.Column(db.String(140), index=True)

    def __repr__(self):
        return '<Post {}>'.format(self.username)

