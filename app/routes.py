from flask import render_template, flash, redirect, url_for, current_app
from app import app
from app.forms import PostForm
from app.models import Post
from app import db
from flask import g
from app.forms import SearchForm
from elasticsearch import Elasticsearch

@app.route('/')
@app.route('/index')
def index():
    posts = Post.query.all()
    return render_template('index.html', title='Home', posts=posts)

@app.route('/postBlog', methods=['GET', 'POST'])
def post():
    form = PostForm()
    if form.validate_on_submit():
        posts = Post(username=str(form.username.data), posts=str(form.post.data))
        db.session.add(posts)
        db.session.commit()
        flash('your Post is now Live!')
        return redirect('/index')
    return render_template('post.html', title='Send', form=form)


@app.route('/search', methods=['GET', 'POST'])
def search():
    search = SearchForm()
    
    es = Elasticsearch("http://localhost:9200")
    posts_all_query = Post.query.all()
    result = list()
    es.indices.create(index='search_index', ignore=400)
    for post_data in posts_all_query:
        payload = {"post":str(post_data.posts)}
        es.index(index='search_index', doc_type='blog',id = post_data.id, body=payload)

    result = es.search(index='search_index', body={"from":0,"size":0, "query":{"match":{"post":search.q.data}}})
    all_result = es.search(index='search_index', body={"from":0,"size":result['hits']['total'], "query":{"match":{"post":search.q.data}}})
    return render_template('search.html', title='Search', result = result, search = search, all_result = all_result)
